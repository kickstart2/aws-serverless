window._config = {
    cognito: {
        userPoolId: 'us-east-2_FPrVC6T6X', // e.g. us-east-2_uXboG5pAb
        userPoolClientId: '25fdf96j1m93nv60u9un46op4h', // e.g. 25ddkmj4v6hfsfvruhpfi7n4hv
        region: 'us-east-2' // e.g. us-east-2
    },
    api: {
        invokeUrl: '' // e.g. https://rc7nyt4tql.execute-api.us-west-2.amazonaws.com/prod',
    }
};
